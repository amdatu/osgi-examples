package net.luminis.shell.example;

import org.osgi.service.component.annotations.Component;

import java.util.Date;

@Component(
        property = {
                "osgi.command.scope=demo",
                "osgi.command.function=time",
                "osgi.command.function=timestamp",
                "osgi.command.function=greet"
        },
        immediate = true,
        service = Object.class
)
public class TimeCommand {

    public void time() {
        System.out.println("The current time is: " + new Date());
    }

    public void timestamp() {
        System.out.println("The current timestamp is: " + System.currentTimeMillis());
    }

    public void greet(String name) {
        System.out.println("Hello " + name);
    }

}

