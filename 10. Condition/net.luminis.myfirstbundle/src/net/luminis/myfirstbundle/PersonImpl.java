package net.luminis.myfirstbundle;

import net.luminis.person.api.Person;
import org.osgi.service.component.annotations.Component;

@Component(property = {
        "author=Xander"
})
public class PersonImpl implements Person {
}
