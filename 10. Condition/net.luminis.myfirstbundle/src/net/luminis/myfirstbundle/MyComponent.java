package net.luminis.myfirstbundle;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.condition.Condition;

@Component
public class MyComponent {

    @Reference(target = "(osgi.condition.id=configured)")
    Condition condition;

    @Activate
    public void start() {
        System.out.println("My component started");
    }

    @Deactivate
    public void stop() {
        System.out.println("My component stopped");
    }
}
