package net.luminis.conditionfactory;

import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.cm.annotations.RequireConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.condition.Condition;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

@RequireConfigurationAdmin
@Component(property = "service.pid=osgi.condition.factory")
public class ConditionFactory implements ManagedServiceFactory {

    private DependencyManager dependencyManager;
    private Map<String, org.apache.felix.dm.Component> conditionComponents = new HashMap<>();

    @Activate
    public void activate(BundleContext bundleContext) {
        this.dependencyManager = new DependencyManager(bundleContext);
    }

    @Override
    public String getName() {
        return "osgi.condition.factory";
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
        String[] matchAllFilters = (String[]) properties.get("osgi.condition.match.all");
        String[] matchNoneFilters = (String[]) properties.get("osgi.condition.match.none");
        Properties conditionProperties = new Properties();
        conditionProperties.setProperty("osgi.condition.id", (String)properties.get("osgi.condition.identifier"));
        org.apache.felix.dm.Component conditionComponent = dependencyManager.createComponent()
                .setInterface(Condition.class, conditionProperties)
                .setImplementation(new ConfiguredCondition(pid, matchNoneFilters))
                .setAutoConfig(org.apache.felix.dm.Component.class, false);
        for (String match : matchAllFilters) {
            conditionComponent.add(dependencyManager.createServiceDependency().setService(match));
        }
        conditionComponents.put(pid, conditionComponent);
        dependencyManager.add(conditionComponent);
    }

    @Override
    public void deleted(String pid) {
        org.apache.felix.dm.Component conditionComponent = conditionComponents.remove(pid);
        dependencyManager.remove(conditionComponent);
    }

    static class ConfiguredCondition implements Condition {

        private final String pid;
        private final String[] matchNoneFilters;
        private volatile org.apache.felix.dm.Component matchNoneComponent;
        private volatile DependencyManager dependencyManager;

        public ConfiguredCondition(String pid, String[] matchNoneFilters) {
            this.pid = pid;
            this.matchNoneFilters = matchNoneFilters;
        }

        void init(org.apache.felix.dm.Component component) {
            Dictionary<String, Object> serviceProperties = new Hashtable<>();
            serviceProperties.put("match.none.available", true);
            serviceProperties.put("service.pid", pid);
            matchNoneComponent = dependencyManager.createComponent()
                    .setImplementation(new MatchNoneComponent(pid))
                    .setInterface(MatchNone.class, serviceProperties);
            if (matchNoneFilters != null) {
                for (String match : matchNoneFilters) {
                    matchNoneComponent.add(dependencyManager.createServiceDependency()
                            .setService(match)
                            .setCallbacks("serviceAdded", "serviceRemoved")
                            .setRequired(false));
                }
            }
            dependencyManager.add(matchNoneComponent);
            component.add(dependencyManager.createServiceDependency()
                    .setService(MatchNone.class, "(&(service.pid=" + pid+ ")(match.none.available=true))")
                    .setAutoConfig(false)
                    .setRequired(true));
        }

        void destroy() {
            dependencyManager.remove(matchNoneComponent);
        }

    }

    static class MatchNoneComponent implements MatchNone {
        private final String pid;
        private AtomicInteger availableDependencyCount = new AtomicInteger();
        private org.apache.felix.dm.Component component;

        public MatchNoneComponent(String pid) {
            this.pid = pid;
        }

        void serviceAdded() {
            int dependencyCount = availableDependencyCount.incrementAndGet();
            updateServiceProperties(dependencyCount);
        }

        void serviceRemoved() {
            int dependencyCount = availableDependencyCount.decrementAndGet();
            updateServiceProperties(dependencyCount);
        }

        private void updateServiceProperties(int dependencyCount) {
            Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
            if (dependencyCount == 0 != (boolean) serviceProperties.get("match.none.available")) {
                serviceProperties.put("match.none.available", dependencyCount == 0);
                component.setServiceProperties(serviceProperties);
            }
        }
    }

    interface MatchNone {

    }
}
