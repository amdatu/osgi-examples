package net.luminis.greeting.publisher;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import java.util.Map;

@Component
public class GreetingPublisher {

    private volatile boolean active;
    private final Thread greetingPublisher;

    @Activate
    public GreetingPublisher(@Reference EventAdmin eventAdmin) {

        active = true;
        greetingPublisher = new Thread(() -> {
            int count = 0;
            while (active) {
                if (count++ % 2 ==0) {
                    eventAdmin.postEvent(new Event("net/luminis/greeting/en", Map.of("greeting", "Hello!")));
                } else {
                    eventAdmin.postEvent(new Event("net/luminis/greeting/nl", Map.of("greeting", "Hallo!")));
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        greetingPublisher.start();
    }

    @Deactivate
    public void deactivate() throws InterruptedException {
        active = false;
        greetingPublisher.join();
    }
}
