package greeter.app;

import greeter.api.Greeter;
//import greeter.helper.Helper;
import greeter.impl.GreeterImpl;

public class Main {

    public static void main(String[] args) {
        Greeter greeter = new GreeterImpl();
        greeter.sayHi();

//        Helper helper = new Helper();
//        helper.help();
    }

}
