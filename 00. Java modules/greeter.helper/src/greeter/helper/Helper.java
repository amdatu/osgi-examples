package greeter.helper;

import greeter.api.Greeter;
import greeter.impl.GreeterImpl;

public class Helper {

    public void help() {
        System.out.println("Helping...");
        Greeter greeter = new GreeterImpl();
        greeter.sayHi();
    }
}
