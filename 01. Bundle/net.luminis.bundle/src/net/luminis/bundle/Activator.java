package net.luminis.bundle;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) {
        System.out.println("Start bundle");
    }

    @Override
    public void stop(BundleContext context) {
        System.out.println("Stop bundle");
    }
}
