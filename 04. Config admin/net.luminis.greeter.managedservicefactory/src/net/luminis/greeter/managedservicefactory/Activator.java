package net.luminis.greeter.managedservicefactory;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedServiceFactory;

import java.util.Dictionary;
import java.util.Hashtable;

public class Activator implements BundleActivator {

    private ServiceRegistration<?> serviceRegistration;
    private GreeterFactory greeterFactory;

    @Override
    public void start(BundleContext context) {
        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put(Constants.SERVICE_PID, "net.luminis.greeter.managedservicefactory");
        greeterFactory = new GreeterFactory(context);
        serviceRegistration = context.registerService(ManagedServiceFactory.class, greeterFactory, properties);
    }

    @Override
    public void stop(BundleContext context) {
        serviceRegistration.unregister();
        greeterFactory.stop();
    }

}
