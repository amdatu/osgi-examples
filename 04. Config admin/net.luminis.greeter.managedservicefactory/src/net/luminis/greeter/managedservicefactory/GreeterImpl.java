package net.luminis.greeter.managedservicefactory;

import net.luminis.greeter.api.Greeter;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.annotations.RequireConfigurationAdmin;

@RequireConfigurationAdmin
public class GreeterImpl implements Greeter {

    private final String greeting;

    public GreeterImpl(String greeting) {
        this.greeting = greeting;
    }

    @Override
    public void sayHi() {
        System.out.println(greeting);
    }

}
