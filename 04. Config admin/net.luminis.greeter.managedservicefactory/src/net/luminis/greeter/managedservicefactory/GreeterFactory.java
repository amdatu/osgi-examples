package net.luminis.greeter.managedservicefactory;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GreeterFactory implements ManagedServiceFactory {

    private final Map<String, ServiceRegistration<Greeter>> greeterServiceRegistrations = new ConcurrentHashMap<>();

    private final BundleContext context;

    public GreeterFactory(BundleContext context) {
        this.context = context;
    }

    @Override
    public String getName() {
        return "Greeter factory";
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties) {
        String greeting = String.valueOf(properties.get("greeting"));
        System.out.println("Creating greeter service with message: '" + greeting + "'");

        ServiceRegistration<Greeter> replaced = greeterServiceRegistrations.put(pid, context.registerService(Greeter.class, new GreeterImpl(greeting), null));
        if (replaced != null) {
            replaced.unregister();
        }
    }

    @Override
    public void deleted(String pid) {
        ServiceRegistration<Greeter> remove = greeterServiceRegistrations.remove(pid);
        if (remove != null) {
            remove.unregister();
        }
    }

    public void stop() {
        greeterServiceRegistrations.values().forEach(ServiceRegistration::unregister);
        greeterServiceRegistrations.clear();
    }
}
