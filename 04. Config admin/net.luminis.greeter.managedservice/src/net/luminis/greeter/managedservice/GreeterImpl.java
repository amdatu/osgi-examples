package net.luminis.greeter.managedservice;

import net.luminis.greeter.api.Greeter;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.annotations.RequireConfigurationAdmin;
import org.osgi.service.configurator.annotations.RequireConfigurator;

import java.util.Dictionary;

@RequireConfigurator // ("resources")
@RequireConfigurationAdmin
public class GreeterImpl implements Greeter, ManagedService {

    private String greeting = "Hi!";

    @Override
    public void sayHi() {
        System.out.println(greeting);
    }

    @Override
    public void updated(Dictionary<String, ?> properties) {
        if (properties != null && properties.get("greeting") != null) {
            String greeting = String.valueOf(properties.get("greeting"));
            System.out.println("Greeting configured new message is: '" + greeting + "'" );
            this.greeting = String.valueOf(greeting);
        } else {
            greeting = "Hi!";
        }
    }

}
