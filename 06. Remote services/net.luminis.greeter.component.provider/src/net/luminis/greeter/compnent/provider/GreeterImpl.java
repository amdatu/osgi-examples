package net.luminis.greeter.compnent.provider;

import net.luminis.greeter.api.Greeter;
import org.osgi.service.component.ComponentConstants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

@Component(
        property = RemoteConstants.SERVICE_EXPORTED_INTERFACES + "=net.luminis.greeter.api.Greeter"
)
public class GreeterImpl implements Greeter {

    @Override
    public void sayHi() {
        System.out.println("Hi!");
    }
}
