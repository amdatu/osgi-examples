package net.luminis.todo.service;

import net.luminis.todo.api.Todo;
import net.luminis.todo.api.TodoService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.transaction.control.TransactionControl;
import org.osgi.service.transaction.control.jpa.JPAEntityManagerProvider;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.stream.Collectors;

@Component(immediate = true)
public class JpaTodoService implements TodoService {

    private final EntityManager entityManager;
    private final TransactionControl transactionControl;

    @Activate
    public JpaTodoService(
            @Reference TransactionControl transactionControl,
            @Reference(target = "(osgi.unit.name=todo)") JPAEntityManagerProvider entityManagerProvider) {
        this.transactionControl = transactionControl;
        entityManager = entityManagerProvider.getResource(transactionControl);

        add("Study OSGi");
        add("Complete the labs");
    }

    @Override
    public Todo add(String description) {
        TodoEntity added = transactionControl.required(() -> {
            TodoEntity todoEntity = new TodoEntity(description);
            entityManager.persist(todoEntity);
            return todoEntity;
        });

        return toTodo(added);
    }

    @Override
    public Todo complete(Long id) {
        TodoEntity completed = transactionControl.required(() -> {
            TodoEntity todoEntity = entityManager.find(TodoEntity.class, id);
            todoEntity.setCompleted(true);
            entityManager.merge(todoEntity);
            return todoEntity;
        });
        return toTodo(completed);
    }


    @Override
    public List<Todo> list() {
        return transactionControl.required(() -> {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<TodoEntity> query = builder.createQuery(TodoEntity.class);
            query.from(TodoEntity.class);
            return entityManager.createQuery(query).getResultList().stream()
                    .map(this::toTodo)
                    .collect(Collectors.toList());
        });

    }

    private Todo toTodo(TodoEntity todoEntity) {
        return new Todo(todoEntity.getId(), todoEntity.getDescription(), todoEntity.getCompleted());
    }


}
