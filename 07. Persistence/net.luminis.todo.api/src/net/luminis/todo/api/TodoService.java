package net.luminis.todo.api;

import java.util.List;

public interface TodoService {

    Todo add(String description);

    Todo complete(Long id);

    List<Todo> list();

}
