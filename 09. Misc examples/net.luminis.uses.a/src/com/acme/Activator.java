package com.acme;

import com.acme.foo.Foo;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("---");
        System.out.println("A is using Foo version " + new Foo() {}.getVersion());
    }

    @Override
    public void stop(BundleContext context) throws Exception {

    }
}
