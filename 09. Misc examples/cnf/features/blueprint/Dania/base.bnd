#
# Amdatu Blueprint base feature
#
blueprint-feature.base: base
blueprint-deps.base: \
	org.apache.felix:org.apache.felix.configadmin:1.9.22,\
	org.apache.felix:org.apache.felix.dependencymanager.annotation:5.0.2,\
	org.apache.felix:org.apache.felix.dependencymanager.runtime:4.0.8,\
	org.apache.felix:org.apache.felix.dependencymanager:4.6.1,\
	org.apache.felix:org.apache.felix.eventadmin:1.6.2,\
	org.apache.felix:org.apache.felix.framework:7.0.1,\
	org.apache.felix:org.apache.felix.metatype:1.2.4,\
	org.ops4j.pax.logging:pax-logging-api:2.0.9,\
	org.ops4j.pax.logging:pax-logging-log4j2:2.0.9,\
	org.osgi:osgi.core:8.0.0,\
	org.osgi:org.osgi.service.cm:1.6.0,\
	org.osgi:org.osgi.service.event:1.4.0,\
	org.osgi:org.osgi.service.log:1.4.0,\
	org.osgi:org.osgi.service.metatype.annotations:1.4.0,\
	org.osgi:org.osgi.service.metatype:1.4.0,\
	org.osgi:osgi.annotation:6.0.1,\
	org.slf4j:slf4j-api:1.7.30

blueprint-deps.test: \
    biz.aQute.bnd:biz.aQute.launchpad:5.3.0,\
	net.bytebuddy:byte-buddy-agent:1.11.3,\
	net.bytebuddy:byte-buddy:1.11.3,\
	org.apache.servicemix.bundles:org.apache.servicemix.bundles.junit:4.12_1,\
	org.junit.jupiter:junit-jupiter-api:${junit-version},\
    org.junit.jupiter:junit-jupiter-params:${junit-version},\
    org.junit.platform:junit-platform-commons:1.7.2,\
    org.junit.platform:junit-platform-launcher:1.7.2,\
    org.junit.vintage:junit-vintage-engine:${junit-version},\
    org.mockito:mockito-core:3.11.2,\
    org.objenesis:objenesis:3.2,\
    org.opentest4j:opentest4j:1.2.0

#
# Build
#
-buildpath.blueprint-base: \
	${if;(buildfeaturesMerged[]=base); \
		org.apache.felix.dependencymanager,\
		org.apache.felix.dependencymanager.annotation,\
		org.osgi.service.cm,\
		org.osgi.service.event,\
		org.osgi.service.metatype,\
		org.osgi.service.metatype.annotations,\
		org.osgi.service.log,\
		osgi.annotation,\
		osgi.core,\
		slf4j.api\
	}

-testpath.blueprint-base: \
    ${if;(buildfeaturesMerged[]=base); \
        biz.aQute.launchpad,\
        org.apache.servicemix.bundles.junit,\
        junit-platform-launcher,\
        junit-platform-commons,\
        junit-jupiter-api,\
        junit-jupiter-params,\
        junit-vintage-engine,\
        org.mockito.mockito-core,\
        org.objenesis,\
        net.bytebuddy.byte-buddy,\
        net.bytebuddy.byte-buddy-agent,\
        org.opentest4j\
    }

#
# Run
#
-runbundles.blueprint-base: \
	${if;(runfeaturesMerged[]=base); \
		org.apache.felix.dependencymanager.runtime,\
		org.apache.felix.dependencymanager,\
		org.apache.felix.configadmin,\
		org.apache.felix.eventadmin,\
		org.apache.felix.metatype,\
		org.ops4j.pax.logging.pax-logging-api,\
		org.ops4j.pax.logging.pax-logging-log4j2\
	}
