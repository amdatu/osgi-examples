package net.luminis.greeter.app;

import net.luminis.greeter.api.GreeterImpl;
import net.luminis.greeter.decorator.GreeterDecorator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    @Override
    public void start(BundleContext context) throws Exception {
        run();
    }

    @Override
    public void stop(BundleContext context) throws Exception {

    }

    void run() {
        new GreeterDecorator().sayDecoratedHi(new GreeterImpl());
    }

    public static void main(String[] args) {
        new Activator().run();
    }

}
