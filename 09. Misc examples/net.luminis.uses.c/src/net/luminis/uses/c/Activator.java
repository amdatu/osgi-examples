package net.luminis.uses.c;

import com.acme.bar.BarImpl;
import com.acme.foo.Foo;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("---");
        System.out.println("C is using Foo version " + new Foo() {}.getVersion());
        System.out.println("C is using Bar version " + new BarImpl().getVersion());
    }

    @Override
    public void stop(BundleContext context) throws Exception {

    }
}
