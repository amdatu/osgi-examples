package com.acme.foo;

public interface Foo {

    default String getVersion() {
        return "2.0 (from D)";
    }

}
