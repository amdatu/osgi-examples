package net.luminis.greeter.test;

import aQute.launchpad.Launchpad;
import aQute.launchpad.LaunchpadBuilder;
import net.luminis.greeter.api.Greeter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class GreeterTest {

    @Mock
    private Greeter greeter;

    @Test
    public void testGreeterProvider() {
        Launchpad launchpad = getLaunchPad("test-provider.bndrun");
        Optional<Greeter> greeterServiceOpt = launchpad.waitForService(Greeter.class, 1000);
        assertTrue(greeterServiceOpt.isPresent());
        greeterServiceOpt.get().sayHi();
    }

    @Test
    public void testGreeterConsumer() {
        Launchpad launchpad = getLaunchPad("test-consumer.bndrun");
        launchpad.register(Greeter.class, greeter);
        Mockito.verify(greeter).sayHi();
    }

    public Launchpad getLaunchPad(String bndrun) {
        return new LaunchpadBuilder()
                .bndrun(bndrun)
                .notestbundle()
                .create(bndrun);
    }
}
