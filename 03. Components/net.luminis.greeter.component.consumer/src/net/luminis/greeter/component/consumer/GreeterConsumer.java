package net.luminis.greeter.component.consumer;

import net.luminis.greeter.api.Greeter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

@Component
public class GreeterConsumer {

    @Activate
    public GreeterConsumer(@Reference Greeter greeter) {
        greeter.sayHi();
    }

    @Deactivate
    public void stop() {
        System.out.println("Greeter consumer deactivated");

    }

}
