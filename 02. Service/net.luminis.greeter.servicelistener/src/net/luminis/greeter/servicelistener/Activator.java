package net.luminis.greeter.servicelistener;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) {

        ServiceListener listener = event -> {
            switch (event.getType()) {
                case ServiceEvent.REGISTERED:
                    System.out.println("Registered " + event.getServiceReference());
                    break;
                case ServiceEvent.MODIFIED:
                    System.out.println("Modified " + event.getServiceReference());
                    break;
                case ServiceEvent.UNREGISTERING:
                    System.out.println("Unregistering " + event.getServiceReference());
                    break;
            }
        };

        context.addServiceListener(listener);

    }

    @Override
    public void stop(BundleContext context) {

    }

}
