package net.luminis.greeter.provider.dutch;


import net.luminis.greeter.api.Greeter;

public class DutchGreeter implements Greeter {

    @Override
    public void sayHi() {
        System.out.println("Hoi!");
    }

}
