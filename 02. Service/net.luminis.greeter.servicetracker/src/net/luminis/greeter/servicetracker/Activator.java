package net.luminis.greeter.servicetracker;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class Activator implements BundleActivator {

    private ServiceTracker<Greeter, Greeter> serviceTracker;

    @Override
    public void start(BundleContext context) {
        serviceTracker = new ServiceTracker<>(context, Greeter.class, null);

        serviceTracker.open();

        serviceTracker.getService().sayHi();

        serviceTracker.close();

    }

    @Override
    public void stop(BundleContext context) {

    }

}
