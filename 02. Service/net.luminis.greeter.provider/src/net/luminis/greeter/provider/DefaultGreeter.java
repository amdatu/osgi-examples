package net.luminis.greeter.provider;


import net.luminis.greeter.api.Greeter;

public class DefaultGreeter implements Greeter {

    @Override
    public void sayHi() {
        System.out.println("Hi!");
    }
}
