package net.luminis.greeter.provider;

import net.luminis.greeter.api.Greeter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

    private volatile ServiceRegistration<Greeter> greeterServiceRegistration;

    @Override
    public void start(BundleContext context) {
        greeterServiceRegistration = context.registerService(Greeter.class, new DefaultGreeter(), null);
    }

    @Override
    public void stop(BundleContext context) {
        greeterServiceRegistration.unregister();
    }
}
